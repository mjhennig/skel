#!/bin/sh
# -------------------------------------------------------------------------
# vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 textwidth=75    :
# -------------------------------------------------------------------------
# This script is meant as a symbolic link to ~/.config/shellrc.           :
# However, many ~/.bashrc's use "test -f" before they include this file,  :
# so that a symbolic link won't work out of the box..                     :
# -------------------------------------------------------------------------
. ~/.config/shellrc

