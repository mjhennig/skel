" -------------------------------------------------------------------------
" vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 textwidth=75    :
" -------------------------------------------------------------------------
" Mathias J. Hennig wrote this file. As long as you retain this notice    :
" you can do whatever you want with this stuff. If we meet some day, and  :
" you think this stuff is worth it, you can buy me a beer in return.      :
" -------------------------------------------------------------------------

set fileencodings=
set nocompatible
"set autochdir
autocmd BufEnter * lcd %:p:h
set autoindent
set background=dark
set backspace=indent,eol,start
"set fileencoding=utf-8
set hlsearch
set showmatch
set ruler
set shiftwidth=4
set tabstop=4
set softtabstop=4
set smartindent
set nocindent
set nocursorcolumn
set nocursorline
set modeline
set modelines=5
set number
set nospell
set noswapfile
set wrap
set linebreak
set tags+=tags;/
set noexpandtab

function! TRIM_SPACES()
    let line = line(".")
    let col = col(".")
    exec "%s/\\s*$//g"
    call cursor(line, col)
endfunc

function! SUPRESS_TABS()
    set expandtab
    set tabstop=4
    set shiftwidth=4
    set softtabstop=4
endfunc

au BufWritePre *.sh,*.ksh,*.bash,*.csh          call TRIM_SPACES()
au BufWritePre *.h,*.hpp,*.hxx,*.c,*.cpp,*.cxx  call TRIM_SPACES()
au BufWritePre *.pl,*.pm,*.py,*.rb,*.php*       call TRIM_SPACES()
au BufWritePre *.htm*,*.tpl,*.txt,*.md          call TRIM_SPACES()
au BufWritePre *.xml,*.xsd,*.wsdl,*.dbk         call TRIM_SPACES()
au BufWritePre *.cmake,*akefile                 call TRIM_SPACES()
au BufWritePre *.vim*                           call TRIM_SPACES()

au! BufWritePost .vimrc source %

au BufReadPre *.sls call SUPRESS_TABS()
au BufNewFile *.sls call SUPRESS_TABS()

hi CursorLine none cterm=bold
set cursorline

map! OH <ESC>0i
map  OH <ESC>0
map! OF <ESC>A
map  OF <ESC>$
map! [3~ <DEL>
map  [3~ <DEL>

